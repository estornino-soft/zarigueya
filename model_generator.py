#!/bin/python3
import os
import json
import re
import argparse
from string import Template

parser = argparse.ArgumentParser(prog="Qt+Go Model Generator", description='Generate the Go code for a Qt model.')

parser.add_argument('model_data', action='store',
                    help='The json file containing model data')

parser.add_argument('-o', '--outpath', default='model_output.go',
                    help='Path of the file where the generated output will be saved in')

parser.add_argument('-t', '--go_template',
                    help='Path of the model template. If nothing is provided, the default files are used. Note that the provided template should support other chosen options, such as --gen-sql')

parser.add_argument('-s', '--gen_sql', action='store_true',
                    help='Whether to generate sql related code.')

parser.add_argument('-v', '--verbose', action='store_true',
                    help='Print replaced strings in the template')

parser.add_argument('--sql_outpath', 
                    help='Path of the .sql output file')

args = parser.parse_args()
script_dir = os.path.dirname(os.path.realpath(__file__))

# If no template is provided, use the default
if args.go_template == None:
    args.go_template = os.path.join(script_dir, 'qtgo_modeltemplate')

    if args.gen_sql:
        args.go_template += '_sql'
    
    args.go_template += '.go'

tmplt_path = args.go_template
json_path = args.model_data
out_path = args.outpath
sqlOut_path = args.sql_outpath
genSql = args.gen_sql
verbose = args.verbose

json_obj = 0

qVariantTypes = {
	"int": "7",
	"uint": "8",
	"int64": "9",
	"uint64": "10",
	"bool": "11",
	"float64": "12",
	"float32": "13",
    "string": "14",
}
qVariantConversions = {
    "int": "ToInt(nil)",
    "string": "ToString()",
}
sqlTypes = {
    "int": "INTEGER",
	"uint": "INTEGER",
	"int64": "INTEGER",
	"uint64": "INTEGER",
	"bool": "INTEGER",
	"float64": "REAL",
	"float32": "REAL",
    "string": "TEXT",
}
emptyConds = {
    "int": "> 0",
    "string": '!= ""',
}

def propReplacements(tmplt, props_def, ix):
    """
    @param props_def: The definition of the properties. A list of dictionaries {"name", "type"}
    @param tmplt: The template for placing the properties data
    @param ix: The index of the property to be replaced
    """
    currentType = props_def[ix]["type"]
    return tmplt.format(
        propName_caps=props_def[ix]["name"].upper(),
        propName_camel=props_def[ix]["name"][0].upper() + props_def[ix]["name"][1:],
        propName_lower=props_def[ix]["name"].lower(),
        propName_var=props_def[ix]["name"][0].lower() + props_def[ix]["name"][1:],
        propType_caps=props_def[ix]["type"].upper(),
        propType_camel=props_def[ix]["type"][0].upper() + props_def[ix]["type"][1:],
        propType_lower=props_def[ix]["type"].lower(),
        propType_var=props_def[ix]["type"][0].lower() + props_def[ix]["type"][1:],
        qVariantType=qVariantTypes[props_def[ix]["type"]],
        convertFromQVariant=qVariantConversions[props_def[ix]["type"]],
        emptyCond=emptyConds[currentType]
    )


def processProperties(props_inst, props_def):
    """
    @param props_inst: The instructions on how to replace qt properties
    @param props_def: The definition of the properties. A list of dictionaries {"name", "type"}
    """
    props_str = ""
    # We can apply three different templates to the properties.
    # One for the first property, one for the last, and a generic
    # one for the rest.
    first_inst = props_inst.get("first", "")
    default_inst = props_inst.get("default", "")
    last_inst = props_inst.get("last", "")

    index_start = 0 if first_inst == "" else 1
    index_end = len(props_def) if last_inst == "" else len(props_def) - 1

    if first_inst != "":
        props_str += propReplacements(first_inst, props_def, 0)
    
    for i in range(index_start, index_end):
        props_str += propReplacements(default_inst, props_def, i)
        
    if last_inst != "":
        props_str += propReplacements(last_inst, props_def, -1)

    if verbose:
        print(props_str)
        print("")
    return props_str

def processInstruction(match):
    # Discard the @ characters at the start and end of the match
    match_str = match.group(0)[1:-1]
    if verbose:
        print(match_str)
    # Read the content as a JSON and save it in a dict
    instructions = json.loads(match_str)
    props_inst = instructions.get("properties", {})
    # If there are instructions for properties, process them
    if props_inst != {}:
        return processProperties(props_inst, json_obj["properties"])

# Conventions:
# <var_name><(S)ingluar|(P)lural>_<caps|camel|lower|var>
with open(tmplt_path, 'r') as tmplt_file,\
        open(json_path, 'r') as json_file:	
    tmplt_str = Template(tmplt_file.read())
    json_obj = json.loads(json_file.read())
    # Replace easy strings like the modelName
    # Default is camel case.
    modelNameS = json_obj["modelNameS"][0].upper() + json_obj["modelNameS"][1:]
    modelNameP = json_obj["modelNameP"][0].upper() + json_obj["modelNameP"][1:]

    out_str = tmplt_str.substitute(
        nameS_caps = modelNameS.upper(),
        nameS_camel = modelNameS,
        nameS_lower = modelNameS.lower(),
        nameS_var = modelNameS[0].lower() + modelNameS[1:],
        nameP_caps = modelNameP.upper(),
        nameP_camel = modelNameP,
        nameP_lower = modelNameP.lower(),
        nameP_var = modelNameP[0].lower() + modelNameP[1:]
    )

    # Now replace the strings that need processing (like loops)
    # The format used in the template is @<instructions-json>@
    # so we use a regexp that matches everything inside @{}@
    regex = r"\@\{(.*?)\}\@"
    out_str = re.sub(regex, processInstruction, out_str, flags=re.MULTILINE | re.DOTALL)

    with open(out_path, 'w') as out_file:
        out_file.write(out_str)
    
    if genSql:
        # If no path for the sql file was provided, save it in the working directory
        if sqlOut_path == None:
            sqlOut_path = os.path.join(os.getcwd(), "{modelname}.sql".format(modelname=modelNameP))

        with open(sqlOut_path, 'w') as out_sql:
            sql_str = "CREATE TABLE IF NOT EXISTS {tableName} (".format(tableName=modelNameP)

            for prop in json_obj["properties"]:
                sql_str += prop["name"] + " " + sqlTypes[prop["type"]] + ", "

            sql_str = sql_str[:-2] + ");\n"

            if "data" in json_obj:
                for entry in json_obj["data"]:
                    sql_str += "INSERT INTO {tableName} VALUES (".format(tableName=modelNameP)

                    for prop in json_obj["properties"]:
                        sql_str += "'{propValue}', ".format(propValue=entry[prop["name"]])

                    sql_str = sql_str[:-2] + ");\n"

                    
            out_sql.write(sql_str)