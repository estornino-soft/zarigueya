package main

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/sql"
	"fmt"
)

const (
	TaskName = int(core.Qt__UserRole) + 1<<iota
	StateColors
	TaskState
	TaskType
	CycleDays
	Position
	TaskValue
	DefaultValue
	Conditions
)

type TasksModel struct {
	core.QAbstractListModel

	_ func() `constructor:"init"`

	_ map[int]*core.QByteArray `property:"roles"`
	_ []*Task `property:"tasks"`

	_ func(taskName string, stateColors string, taskState string, taskType string, cycleDays int, position int, taskValue string, defaultValue string, conditions string) `slot:"addTask"`
	_ func(*Task) `slot:"addTask2"`
	// map definitions have no spaces! Adding a space might fuck up the
	// moc generation. Same goes with comments, this section is very sensitive!
	//map[string]string,
	_ func(index int, taskName string, stateColors string, taskState string, taskType string, cycleDays int, position int, taskValue string, defaultValue string, conditions string) `slot:"editTask"`
	_ func(index int) `slot:"removeTask"`
	ReadTasksFromSqliteDb func() bool

	DbConnectName string
}

type Task struct {
	core.QObject
	_ string `property:"rowid"`
	_ string `property:"taskName"`
	_ string `property:"stateColors"`
	_ string `property:"taskState"`
	_ string `property:"taskType"`
	_ int `property:"cycleDays"`
	_ int `property:"position"`
	_ string `property:"taskValue"`
	_ string `property:"defaultValue"`
	_ string `property:"conditions"`
}

func init() {
	Task_QRegisterMetaType()
}

func (m *TasksModel) init() {
	m.SetRoles(map[int]*core.QByteArray{
	TaskName: core.NewQByteArray2("taskName", len("taskName")),
	StateColors: core.NewQByteArray2("stateColors", len("stateColors")),
	TaskState: core.NewQByteArray2("taskState", len("taskState")),
	TaskType: core.NewQByteArray2("taskType", len("taskType")),
	CycleDays: core.NewQByteArray2("cycleDays", len("cycleDays")),
	Position: core.NewQByteArray2("position", len("position")),
	TaskValue: core.NewQByteArray2("taskValue", len("taskValue")),
	DefaultValue: core.NewQByteArray2("defaultValue", len("defaultValue")),
	Conditions: core.NewQByteArray2("conditions", len("conditions")),

	})

	m.ConnectData(m.data)
	m.ConnectRowCount(m.rowCount)
	m.ConnectColumnCount(m.columnCount)
	m.ConnectRoleNames(m.roleNames)

	m.ConnectAddTask(m.addTask)
	m.ConnectAddTask2(m.addTask2)
	m.ConnectEditTask(m.editTask)
	m.ConnectRemoveTask(m.removeTask)
}

func (m *TasksModel) data(index *core.QModelIndex, role int) *core.QVariant {
	if !index.IsValid() {
		fmt.Println("Index not valid:", index.Row(), index.Column())
		return core.NewQVariant()
	}

	/*
	if role != int(core.Qt__DisplayRole) {
		fmt.Println("Role is not DisplayRole", role, core.Qt__DisplayRole)
		return core.NewQVariant()
	}*/

	if index.Row() >= len(m.Tasks()) {
		fmt.Println("Row value too big")
		return core.NewQVariant()
	}

	var task = m.Tasks()[index.Row()]

	/*
	To know which number you should use in the constructor
	NewQVariant<number>, visit the official QVariant docs.
	Numbers start at 5 from QVariant(const QVariant &p).
	Numbers 1-4 are:
	1 - Doesn't exist
	2 - QVariantType
	3 - (int, unsafe.Pointer)
	4 - undefined

	5 - QVariant_ITF
	6 - QDataStream_ITF
	7 - int
	8 - uint
	9 - int64
	10 - uint64
	11 - bool
	12 - float64
	13 - float32
	14 - string
	15 - QByteArray
	...
	*/
	switch role {
	case TaskName:
		return core.NewQVariant14(task.TaskName())
	case StateColors:
		return core.NewQVariant14(task.StateColors())
	case TaskState:
		return core.NewQVariant14(task.TaskState())
	case TaskType:
		return core.NewQVariant14(task.TaskType())
	case CycleDays:
		return core.NewQVariant7(task.CycleDays())
	case Position:
		return core.NewQVariant7(task.Position())
	case TaskValue:
		return core.NewQVariant14(task.TaskValue())
	case DefaultValue:
		return core.NewQVariant14(task.DefaultValue())
	case Conditions:
		return core.NewQVariant14(task.Conditions())

	}

	return task.ToVariant()
}

func (m *TasksModel) rowCount(parent *core.QModelIndex) int {
	return len(m.Tasks())
}

func (m *TasksModel) columnCount(parent *core.QModelIndex) int {
	return 1
}

func (m *TasksModel) roleNames() map[int]*core.QByteArray {
	return m.Roles()
}

func (m *TasksModel) addTask(taskName string,
		stateColors string,
		taskState string,
		taskType string,
		cycleDays int,
		position int,
		taskValue string,
		defaultValue string,
		conditions string) {
	task := NewTask(nil)
	task.SetTaskName(taskName)
	task.SetStateColors(stateColors)
	task.SetTaskState(taskState)
	task.SetTaskType(taskType)
	task.SetCycleDays(cycleDays)
	task.SetPosition(position)
	task.SetTaskValue(taskValue)
	task.SetDefaultValue(defaultValue)
	task.SetConditions(conditions)

	m.addTask2(task)
}

func (m *TasksModel) addTask2(task *Task) {
	// beginInsertColumns(const QModelIndex &parent, int first, int last)
	m.BeginInsertRows(core.NewQModelIndex(), len(m.Tasks()), len(m.Tasks()))
	defer m.EndInsertRows()

	query := sql.NewQSqlQuery3(sql.QSqlDatabase_Database(m.DbConnectName, true))
	query.Prepare("INSERT INTO tasks (taskName, stateColors, taskState, taskType, cycleDays, position, taskValue, defaultValue, conditions) VALUES (:taskName, :stateColors, :taskState, :taskType, :cycleDays, :position, :taskValue, :defaultValue, :conditions);")
	query.BindValue(":taskName", core.NewQVariant14(task.TaskName()), sql.QSql__In)
	query.BindValue(":stateColors", core.NewQVariant14(task.StateColors()), sql.QSql__In)
	query.BindValue(":taskState", core.NewQVariant14(task.TaskState()), sql.QSql__In)
	query.BindValue(":taskType", core.NewQVariant14(task.TaskType()), sql.QSql__In)
	query.BindValue(":cycleDays", core.NewQVariant7(task.CycleDays()), sql.QSql__In)
	query.BindValue(":position", core.NewQVariant7(task.Position()), sql.QSql__In)
	query.BindValue(":taskValue", core.NewQVariant14(task.TaskValue()), sql.QSql__In)
	query.BindValue(":defaultValue", core.NewQVariant14(task.DefaultValue()), sql.QSql__In)
	query.BindValue(":conditions", core.NewQVariant14(task.Conditions()), sql.QSql__In)

	fmt.Println(query.LastQuery())
	if !query.Exec2() {
		fmt.Println("An error occurred adding task:", query.LastError().Text())
		return
	}

	task.SetRowid(query.LastInsertId().ToString())

	m.SetTasks(append(m.Tasks(), task))
}

func (m *TasksModel) editTask( row int,
		taskName string,
		stateColors string,
		taskState string,
		taskType string,
		cycleDays int,
		position int,
		taskValue string,
		defaultValue string,
		conditions string) {
	var task = m.Tasks()[row]

	if taskName != "" {
		task.SetTaskName(taskName)
	}
	if stateColors != "" {
		task.SetStateColors(stateColors)
	}
	if taskState != "" {
		task.SetTaskState(taskState)
	}
	if taskType != "" {
		task.SetTaskType(taskType)
	}
	if cycleDays > 0 {
		task.SetCycleDays(cycleDays)
	}
	if position > 0 {
		task.SetPosition(position)
	}
	if taskValue != "" {
		task.SetTaskValue(taskValue)
	}
	if defaultValue != "" {
		task.SetDefaultValue(defaultValue)
	}
	if conditions != "" {
		task.SetConditions(conditions)
	}


	query := sql.NewQSqlQuery3(sql.QSqlDatabase_Database(m.DbConnectName, true))
	//TODO: Verificar el template de funciones add, edit, remove.
	query.Prepare("UPDATE tasks SET taskName = :taskName, stateColors = :stateColors, taskState = :taskState, taskType = :taskType, cycleDays = :cycleDays, position = :position, taskValue = :taskValue, defaultValue = :defaultValue, conditions = :conditions WHERE rowid = '" + task.Rowid() + "';")
	//query_str := "UPDATE tasks SET taskName = '" + task.TaskName() + "', " + "stateColors = '" + task.StateColors() + "', " + "taskState = '" + task.TaskState() + "', " + "taskType = '" + task.TaskType() + "', " + "cycleDays = '" + task.CycleDays() + "', " + "position = '" + task.Position() + "', " + "taskValue = '" + task.TaskValue() + "', " + "defaultValue = '" + task.DefaultValue() + "', " + "conditions = '" + task.Conditions() + "' WHERE rowid = '" + task.Rowid() + "';"
	query.BindValue(":taskName", core.NewQVariant14(task.TaskName()), sql.QSql__In)
	query.BindValue(":stateColors", core.NewQVariant14(task.StateColors()), sql.QSql__In)
	query.BindValue(":taskState", core.NewQVariant14(task.TaskState()), sql.QSql__In)
	query.BindValue(":taskType", core.NewQVariant14(task.TaskType()), sql.QSql__In)
	query.BindValue(":cycleDays", core.NewQVariant7(task.CycleDays()), sql.QSql__In)
	query.BindValue(":position", core.NewQVariant7(task.Position()), sql.QSql__In)
	query.BindValue(":taskValue", core.NewQVariant14(task.TaskValue()), sql.QSql__In)
	query.BindValue(":defaultValue", core.NewQVariant14(task.DefaultValue()), sql.QSql__In)
	query.BindValue(":conditions", core.NewQVariant14(task.Conditions()), sql.QSql__In)

	fmt.Println(query.LastQuery())
	if !query.Exec2() {
		fmt.Println("An error occurred editing task ", row, query.LastError().Text())
		return
	}
	var taskIndex = m.Index(row, 0, core.NewQModelIndex())
	//dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
	//			  const QVector<int> &roles = ...)

	m.DataChanged(taskIndex, taskIndex, []int{TaskName, StateColors, TaskState, TaskType, CycleDays, Position, TaskValue, DefaultValue, Conditions})
}

func (m *TasksModel) removeTask(row int) {
	m.BeginRemoveRows(core.NewQModelIndex(), row, row)
	defer m.EndRemoveRows()

	query := sql.NewQSqlQuery3(sql.QSqlDatabase_Database(m.DbConnectName, true))
	query_str := "DELETE FROM tasks WHERE rowid = '" + m.Tasks()[row].Rowid() + "';"
	if !query.Exec(query_str) {
		fmt.Println("An error occurred deleting task ", row, query.LastError().Text())
		return
	}

	m.SetTasks(append(m.Tasks()[:row], m.Tasks()[row+1:]...))
}

func (m *TasksModel) ReadTasksFromDb () bool {
	query := sql.NewQSqlQuery3(sql.QSqlDatabase_Database(m.DbConnectName, true))
	query_str := "SELECT rowid, * FROM tasks;"
	var tasks []*Task
	if query.Exec(query_str) {
		for query.Next() {
			task := NewTask(nil)
			record := query.Record()
			task.SetRowid(query.Value(record.IndexOf("rowid")).ToString())
			task.SetTaskName(query.Value(record.IndexOf("taskName")).ToString())
			task.SetStateColors(query.Value(record.IndexOf("stateColors")).ToString())
			task.SetTaskState(query.Value(record.IndexOf("taskState")).ToString())
			task.SetTaskType(query.Value(record.IndexOf("taskType")).ToString())
			task.SetCycleDays(query.Value(record.IndexOf("cycleDays")).ToInt(nil))
			task.SetPosition(query.Value(record.IndexOf("position")).ToInt(nil))
			task.SetTaskValue(query.Value(record.IndexOf("taskValue")).ToString())
			task.SetDefaultValue(query.Value(record.IndexOf("defaultValue")).ToString())
			task.SetConditions(query.Value(record.IndexOf("conditions")).ToString())


			tasks = append(tasks, task)
		}

		m.SetTasks(tasks)
		return true
	} 
	
	fmt.Println("An error occurred when fetching tasks from db with query '" + query_str + "':", query.LastError().Text())
	return false
}