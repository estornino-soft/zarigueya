CREATE TABLE IF NOT EXISTS Tasks (taskName TEXT, stateColors TEXT, taskState TEXT, taskType TEXT, cycleDays INTEGER, position INTEGER, taskValue TEXT, defaultValue TEXT, conditions TEXT);
INSERT INTO Tasks VALUES ('Kungfu', 'Empty:white,Done:sienna', 'Empty', 'simple', '1', '1', '', '', '');
INSERT INTO Tasks VALUES ('Tong Zho Gong', 'Empty:white,Done:springgreen', 'Empty', 'simple', '1', '2', '', '', '');
INSERT INTO Tasks VALUES ('Organic Trash', 'Empty:white,Done:orchid,Failed:brown', 'Empty', 'ratio', '2', '3', '', '', 'default');
