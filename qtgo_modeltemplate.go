package main

import (
	"github.com/therecipe/qt/core"
	"fmt"
)

const (
	@{"properties": {"first": "{propName_camel} = int(core.Qt__UserRole) + 1<<iota\n", "default": "\t{propName_camel}\n", "last": "\t{propName_camel}"}}@
)

type ${nameP_camel}Model struct {
	core.QAbstractListModel

	_ func() `constructor:"init"`

	_ map[int]*core.QByteArray `property:"roles"`
	_ []*${nameS_camel} `property:"${nameP_lower}"`

	_ func(*${nameS_camel}) `slot:"add${nameS_camel}"`
	// map definitions have no spaces! Adding a space might fuck up the
	// moc generation. Same goes with comments, this section is very sensitive!
	//map[string]string,
	_ func(index int, @{"properties": {"default": "{propName_var} {propType_var}, ", "last": "{propName_var} {propType_var}"}}@) `slot:"edit${nameS_camel}"`
	_ func(index int) `slot:"remove${nameS_camel}"`
}

type ${nameS_camel} struct {
	core.QObject
@{"properties": {"default": "\t_ {propType_var} `property:\"{propName_var}\"`\n", "last": "\t_ {propType_var} `property:\"{propName_var}\"`"}}@
}

func init() {
	${nameS_camel}_QRegisterMetaType()
}

func (m *${nameP_camel}Model) init() {
	m.SetRoles(map[int]*core.QByteArray{
	@{"properties": {"default": "\t{propName_camel}: core.NewQByteArray2(\"{propName_var}\", len(\"{propName_var}\")),\n"}}@
	})

	m.ConnectData(m.data)
	m.ConnectRowCount(m.rowCount)
	m.ConnectColumnCount(m.columnCount)
	m.ConnectRoleNames(m.roleNames)

	m.ConnectAdd${nameS_camel}(m.add${nameS_camel})
	m.ConnectEdit${nameS_camel}(m.edit${nameS_camel})
	m.ConnectRemove${nameS_camel}(m.remove${nameS_camel})
}

func (m *${nameP_camel}Model) data(index *core.QModelIndex, role int) *core.QVariant {
	if !index.IsValid() {
		fmt.Println("Index not valid:", index.Row(), index.Column())
		return core.NewQVariant()
	}

	/*
	if role != int(core.Qt__DisplayRole) {
		fmt.Println("Role is not DisplayRole", role, core.Qt__DisplayRole)
		return core.NewQVariant()
	}*/

	if index.Row() >= len(m.${nameP_camel}()) {
		fmt.Println("Row value too big")
		return core.NewQVariant()
	}

	var ${nameS_lower} = m.${nameP_camel}()[index.Row()]

	/*
	To know which number you should use in the constructor
	NewQVariant<number>, visit the official QVariant docs.
	Numbers start at 5 from QVariant(const QVariant &p).
	Numbers 1-4 are:
	1 - Doesn't exist
	2 - QVariantType
	3 - (int, unsafe.Pointer)
	4 - undefined

	5 - QVariant_ITF
	6 - QDataStream_ITF
	7 - int
	8 - uint
	9 - int64
	10 - uint64
	11 - bool
	12 - float64
	13 - float32
	14 - string
	15 - QByteArray
	...
	*/
	switch role {
@{"properties": {"default": "\tcase {propName_camel}:\n\t\treturn core.NewQVariant{qVariantType}(${nameS_lower}.{propName_camel}())\n"}}@
	}

	return ${nameS_lower}.ToVariant()
}

func (m *${nameP_camel}Model) rowCount(parent *core.QModelIndex) int {
	return len(m.${nameP_camel}())
}

func (m *${nameP_camel}Model) columnCount(parent *core.QModelIndex) int {
	return 1
}

func (m *${nameP_camel}Model) roleNames() map[int]*core.QByteArray {
	return m.Roles()
}

func (m *${nameP_camel}Model) add${nameS_camel}(${nameS_lower} *${nameS_camel}) {
	// beginInsertColumns(const QModelIndex &parent, int first, int last)
	m.BeginInsertRows(core.NewQModelIndex(), len(m.${nameP_camel}()), len(m.${nameP_camel}()))
	defer m.EndInsertRows()

	m.Set${nameP_camel}(append(m.${nameP_camel}(), ${nameS_lower}))
}

func (m *${nameP_camel}Model) edit${nameS_camel}(row int,
@{"properties": {"default": "\t\t\t\t\t\t\t{propName_var} {propType_var},\n", "last": "\t\t\t\t\t\t\t{propName_var} {propType_var}"}}@) {
	var ${nameS_lower} = m.${nameP_camel}()[row]

@{"properties": {"default": "\tif {propName_var} {emptyCond} {{\n\t\t${nameS_lower}.Set{propName_camel}({propName_var})\n\t}}\n"}}@

	var ${nameS_lower}Index = m.Index(row, 0, core.NewQModelIndex())
	//dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
	//			  const QVector<int> &roles = ...)
	m.DataChanged(${nameS_lower}Index, ${nameS_lower}Index, []int{@{"properties": {"default": "{propName_camel}, ","last": "{propName_camel}" }}@})
}

func (m *${nameP_camel}Model) remove${nameS_camel}(row int) {
	m.BeginRemoveRows(core.NewQModelIndex(), row, row)
	defer m.EndRemoveRows()
	m.Set${nameP_camel}(append(m.${nameP_camel}()[:row], m.${nameP_camel}()[row+1:]...))
}