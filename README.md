# Zarigueya

A set of tools to bootstrap your go + qt project.

## Creating a Qt Model

1. Create a JSON file that contains the details of the model. A template of such file is in `modeldetails.json`. An example can be found at `examples/task_modeldetails.json` Fields must be:

* "modelNameS": The name of the model in singular
* "modelNameP": The name of the model in plural
* "properties": A list of the model properties. Each element is an object, with fields "name" and "type". "type" must be a valid Qt type.

2. Run `format_qtgo_template.py` with the corresponding arguments:

```
positional arguments:
  model_data            The json file containing model data

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPATH, --outpath OUTPATH
                        Path of the file where the generated output will be
                        saved in
  -t GO_TEMPLATE, --go_template GO_TEMPLATE
                        Path of the model template. If nothing is provided,
                        the default files are used. Note that the provided
                        template should support other chosen options, such as
                        --gen-sql
  -s, --gen_sql         Whether to generate sql related code.
  -v, --verbose         Print replaced strings in the template
  --sql_outpath SQL_OUTPATH
                        Path of the .sql output file
```