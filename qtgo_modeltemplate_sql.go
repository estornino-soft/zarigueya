package main

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/sql"
	"fmt"
)

const (
	@{"properties": {"first": "{propName_camel} = int(core.Qt__UserRole) + 1<<iota\n", "default": "\t{propName_camel}\n", "last": "\t{propName_camel}"}}@
)

type ${nameP_camel}Model struct {
	core.QAbstractListModel

	_ func() `constructor:"init"`

	_ map[int]*core.QByteArray `property:"roles"`
	_ []*${nameS_camel} `property:"${nameP_var}"`

	_ func(@{"properties": {"default": "{propName_var} {propType_var}, ", "last": "{propName_var} {propType_var}"}}@) `slot:"add${nameS_camel}"`
	_ func(*${nameS_camel}) `slot:"add${nameS_camel}2"`
	// map definitions have no spaces! Adding a space might fuck up the
	// moc generation. Same goes with comments, this section is very sensitive!
	//map[string]string,
	_ func(index int, @{"properties": {"default": "{propName_var} {propType_var}, ", "last": "{propName_var} {propType_var}"}}@) `slot:"edit${nameS_camel}"`
	_ func(index int) `slot:"remove${nameS_camel}"`
	ReadTasksFromSqliteDb func() bool

	DbConnectName string
}

type ${nameS_camel} struct {
	core.QObject
	_ string `property:"rowid"`
@{"properties": {"default": "\t_ {propType_var} `property:\"{propName_var}\"`\n", "last": "\t_ {propType_var} `property:\"{propName_var}\"`"}}@
}

func init() {
	${nameS_camel}_QRegisterMetaType()
}

func (m *${nameP_camel}Model) init() {
	m.SetRoles(map[int]*core.QByteArray{
@{"properties": {"default": "\t{propName_camel}: core.NewQByteArray2(\"{propName_var}\", len(\"{propName_var}\")),\n"}}@
	})

	m.ConnectData(m.data)
	m.ConnectRowCount(m.rowCount)
	m.ConnectColumnCount(m.columnCount)
	m.ConnectRoleNames(m.roleNames)

	m.ConnectAdd${nameS_camel}(m.add${nameS_camel})
	m.ConnectAdd${nameS_camel}2(m.add${nameS_camel}2)
	m.ConnectEdit${nameS_camel}(m.edit${nameS_camel})
	m.ConnectRemove${nameS_camel}(m.remove${nameS_camel})
}

func (m *${nameP_camel}Model) data(index *core.QModelIndex, role int) *core.QVariant {
	if !index.IsValid() {
		fmt.Println("Index not valid:", index.Row(), index.Column())
		return core.NewQVariant()
	}

	/*
	if role != int(core.Qt__DisplayRole) {
		fmt.Println("Role is not DisplayRole", role, core.Qt__DisplayRole)
		return core.NewQVariant()
	}*/

	if index.Row() >= len(m.${nameP_camel}()) {
		fmt.Println("Row value too big")
		return core.NewQVariant()
	}

	var ${nameS_var} = m.${nameP_camel}()[index.Row()]

	/*
	To know which number you should use in the constructor
	NewQVariant<number>, visit the official QVariant docs.
	Numbers start at 5 from QVariant(const QVariant &p).
	Numbers 1-4 are:
	1 - Doesn't exist
	2 - QVariantType
	3 - (int, unsafe.Pointer)
	4 - undefined

	5 - QVariant_ITF
	6 - QDataStream_ITF
	7 - int
	8 - uint
	9 - int64
	10 - uint64
	11 - bool
	12 - float64
	13 - float32
	14 - string
	15 - QByteArray
	...
	*/
	switch role {
@{"properties": {"default": "\tcase {propName_camel}:\n\t\treturn core.NewQVariant{qVariantType}(${nameS_var}.{propName_camel}())\n"}}@
	}

	return ${nameS_var}.ToVariant()
}

func (m *${nameP_camel}Model) rowCount(parent *core.QModelIndex) int {
	return len(m.${nameP_camel}())
}

func (m *${nameP_camel}Model) columnCount(parent *core.QModelIndex) int {
	return 1
}

func (m *${nameP_camel}Model) roleNames() map[int]*core.QByteArray {
	return m.Roles()
}

func (m *${nameP_camel}Model) add${nameS_camel}(@{"properties": {"first": "{propName_var} {propType_var},\n", "default": "\t\t{propName_var} {propType_var},\n", "last": "\t\t{propName_var} {propType_var}"}}@) {
	${nameS_var} := New${nameS_camel}(nil)
@{"properties": {"default": "\t${nameS_var}.Set{propName_camel}({propName_var})\n"}}@
	m.add${nameS_camel}2(${nameS_var})
}

func (m *${nameP_camel}Model) add${nameS_camel}2(${nameS_var} *${nameS_camel}) {
	// beginInsertColumns(const QModelIndex &parent, int first, int last)
	m.BeginInsertRows(core.NewQModelIndex(), len(m.${nameP_camel}()), len(m.${nameP_camel}()))
	defer m.EndInsertRows()

	query := sql.NewQSqlQuery3(sql.QSqlDatabase_Database(m.DbConnectName, true))
	query.Prepare("INSERT INTO ${nameP_var} (@{"properties": {"default": "{propName_var}, ", "last": "{propName_var}"}}@) VALUES (@{"properties": {"default": ":{propName_var}, ", "last": ":{propName_var}"}}@);")
@{"properties": {"default": "\tquery.BindValue(\":{propName_var}\", core.NewQVariant{qVariantType}(${nameS_var}.{propName_camel}()), sql.QSql__In)\n"}}@
	fmt.Println(query.LastQuery())
	if !query.Exec2() {
		fmt.Println("An error occurred adding ${nameS_var}:", query.LastError().Text())
		return
	}

	${nameS_var}.SetRowid(query.LastInsertId().ToString())

	m.Set${nameP_camel}(append(m.${nameP_camel}(), ${nameS_var}))
}

func (m *${nameP_camel}Model) edit${nameS_camel}( row int,
@{"properties": {"default": "\t\t{propName_var} {propType_var},\n", "last": "\t\t{propName_var} {propType_var}"}}@) {
	var ${nameS_var} = m.${nameP_camel}()[row]

@{"properties": {"default": "\tif {propName_var} {emptyCond} {{\n\t\t${nameS_var}.Set{propName_camel}({propName_var})\n\t}}\n"}}@

	query := sql.NewQSqlQuery3(sql.QSqlDatabase_Database(m.DbConnectName, true))
	//TODO: Verificar el template de funciones add, edit, remove.
	query.Prepare("UPDATE ${nameP_var} SET @{"properties": {"default": "{propName_var} = :{propName_var}, ", "last": "{propName_var} = :{propName_var}"}}@ WHERE rowid = '" + ${nameS_var}.Rowid() + "';")
	//query_str := "UPDATE ${nameP_var} SET @{"properties": {"default": "{propName_var} = '\" + ${nameS_var}.{propName_camel}() + \"', \" + \"", "last": "{propName_var} = '\" + ${nameS_var}.{propName_camel}()"}}@ + "' WHERE rowid = '" + ${nameS_var}.Rowid() + "';"
@{"properties": {"default": "\tquery.BindValue(\":{propName_var}\", core.NewQVariant{qVariantType}(${nameS_var}.{propName_camel}()), sql.QSql__In)\n"}}@
	fmt.Println(query.LastQuery())
	if !query.Exec2() {
		fmt.Println("An error occurred editing ${nameS_var} ", row, query.LastError().Text())
		return
	}
	var ${nameS_var}Index = m.Index(row, 0, core.NewQModelIndex())
	//dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
	//			  const QVector<int> &roles = ...)

	m.DataChanged(${nameS_var}Index, ${nameS_var}Index, []int{@{"properties": {"default": "{propName_camel}, ","last": "{propName_camel}" }}@})
}

func (m *${nameP_camel}Model) remove${nameS_camel}(row int) {
	m.BeginRemoveRows(core.NewQModelIndex(), row, row)
	defer m.EndRemoveRows()

	query := sql.NewQSqlQuery3(sql.QSqlDatabase_Database(m.DbConnectName, true))
	query_str := "DELETE FROM ${nameP_var} WHERE rowid = '" + m.${nameP_camel}()[row].Rowid() + "';"
	if !query.Exec(query_str) {
		fmt.Println("An error occurred deleting ${nameS_var} ", row, query.LastError().Text())
		return
	}

	m.Set${nameP_camel}(append(m.${nameP_camel}()[:row], m.${nameP_camel}()[row+1:]...))
}

func (m *${nameP_camel}Model) Read${nameP_camel}FromDb () bool {
	query := sql.NewQSqlQuery3(sql.QSqlDatabase_Database(m.DbConnectName, true))
	query_str := "SELECT rowid, * FROM ${nameP_var};"
	var ${nameP_var} []*${nameS_camel}
	if query.Exec(query_str) {
		for query.Next() {
			${nameS_var} := New${nameS_camel}(nil)
			record := query.Record()
			${nameS_var}.SetRowid(query.Value(record.IndexOf("rowid")).ToString())
@{"properties": {"default": "\t\t\t${nameS_var}.Set{propName_camel}(query.Value(record.IndexOf(\"{propName_var}\")).{convertFromQVariant})\n"}}@

			${nameP_var} = append(${nameP_var}, ${nameS_var})
		}

		m.Set${nameP_camel}(${nameP_var})
		return true
	} 
	
	fmt.Println("An error occurred when fetching tasks from db with query '" + query_str + "':", query.LastError().Text())
	return false
}